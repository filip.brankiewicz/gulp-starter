<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Bony za opony!</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap&subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="./dist/css/global.css">

</head>

<body>
    <header class="header section">
        <div class="header__wrapper wrapper">
            <a href="http://www.latexopony.pl" class="header__link" target="_blank">
                <img src="./dist/images/latex-big.png" alt="Latex Opony" class="header__logo header__logo--main">
            </a>
            <a href="https://serwis500.pl" class="header__link" target="_blank">
                <img src="./dist/images/serwis-500.png" alt="Serwis 500" class="header__logo header__logo--side">
            </a>
            <a href="https://xpartner.net.pl" class="header__link" target="_blank">
                <img src="./dist/images/xpartner.png" alt="xPartner" class="header__logo header__logo--side">
            </a>
        </div>
    </header>
    <main class="main section">
        <div class="main__wrapper main__wrapper--center wrapper">
            <div class="main__column">
                <h2 class="main__subheader">Potwierdzenie zgłoszenia przebiegło pomyślnie!</h2>
                <?php
                include '/home4/r2884/domains/konkurs.xpartner.pl/config/form.php';

                $conn = new mysqli($servername, $username, $password, $database);

                if (isset($_GET['email']) && !empty($_GET['email']) and isset($_GET['hash']) && !empty($_GET['hash'])) {
                    $email = $_GET['email'];
                    $hash = $_GET['hash'];

                    $sql = "UPDATE kumho SET active='1' WHERE email='" . $email . "' AND hash='" . $hash . "' AND active='0'";
                    $conn->query($sql);
                    $conn->close();
                } else { }

                ?>
                <h1 class="main__header-wrapper">
                    <p class="main__header main__header--red main__header--big">BONY</p>
                    <p class="main__header main__header--blue main__header--small">ZA OPONY!</p>
                </h1>
            </div>
        </div>
    </main>
    <footer class="footer section">
        <div class="footer__wrapper wrapper">
            <div class="footer__top">
                <p class="footer__text">Akcja trwa<br />w dniach:</p>
                <p class="footer__frame">01.08-31.12.2019</p>
            </div>
        </div>
    </footer>
    <script src="./dist/js/index.js"></script>
</body>

</html>