"use strict";

$(document).ready(function () {
  var close = $(".thanks__close");
  close.click(function () {
    $(".thanks-wrapper").fadeOut();
  });
});