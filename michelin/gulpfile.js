var gulp = require("gulp");
var sass = require("gulp-sass");
var babel = require("gulp-babel");
var browserSync = require("browser-sync").create();

gulp.task("styles", function() {
    return gulp
        .src("./src/style/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest("./dist/css/"));
});

gulp.task("scripts", function() {
    return gulp
        .src("./src/js/index.js")
        .pipe(
            babel({
                presets: ["@babel/env"]
            })
        )
        .pipe(gulp.dest("./dist/js/"));
});

gulp.task("default", function() {
    browserSync.init({
        proxy: "http://localhost/latex/michelin/",
        notify: false
    });
    gulp.watch("./*.html").on("change", browserSync.reload);
    gulp.watch("./*.php").on("change", browserSync.reload);
    gulp.watch("./*/*.php").on("change", browserSync.reload);
    gulp.watch("./dist/").on("change", browserSync.reload);
    gulp.watch("./src/style/**/*.scss", gulp.series("styles"));
    gulp.watch("./src/js/index.js", gulp.series("scripts"));
});
