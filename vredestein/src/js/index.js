$(document).ready(() => {
    const close = $(".thanks__close");

    close.click(() => {
        $(".thanks-wrapper").fadeOut();
    });
});
