<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Toyo</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <meta name="robots" content="noindex">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap&subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="./dist/css/global.css">

</head>

<body>
    <header class="header section">
        <div class="header__wrapper wrapper">
            <a href="" class="header__link">
                <img src="./dist/images/latex.png" alt="" class="header__logo header__logo--main">
            </a>
            <a href="" class="header__link">
                <img src="./dist/images/toyo.png" alt="" class="header__logo header__logo--side">
            </a>
        </div>
    </header>
    <main class="main section">
        <div class="main__wrapper wrapper">
            <div class="main__content">
                <h1 class="main__header-wrapper">
                    <p class="main__header main__header--red main__header--big">BONY</p>
                    <p class="main__header main__header--blue main__header--small">ZA OPONY!</p>
                </h1>
                <p class="main__text main__text--big">Zyskuj dodatkowe korzyści zwiększając liczbę sprzedanych
                    opon.<br /> Odbierz bony zakupowe, które wykorzystasz
                    w&nbsp;sklepach na terenie całego kraju.</p>
                <p class="main__text main__text--small">Szczegółowe informacje na temat promocji znajdziesz w
                    regulaminie. Wypełnij uważnie formularz zgłoszeniowy i zaznacz zgody
                    na&nbsp;zarządzanie Twoimi danymi a następnie potwierdź udział w&nbsp;akcji
                    w&nbsp;e&#8209;mailu.</p>
            </div>
            <form name="form" class="form" method="post" onsubmit="return validateForm()" action="">
                <div class="form__wrapper">
                    <h3 class="form__header">Formularz zgłoszeniowy</h3>
                    <div class="form__content">
                        <label for="firstName" class="form__label">
                            <p class="form__error">Pole nie może być puste</p>
                            <input id="firstName" name="firstName" type="text" class="form__input" placeholder="none">
                            <p class="form__placeholder">Imię<span class="form__placeholder--red">*</span>
                            </p>
                        </label>
                        <label for="lastName" class="form__label">
                            <p class="form__error">Pole nie może być puste</p>
                            <input id="lastName" name="lastName" type="text" class="form__input" placeholder="none">
                            <p class="form__placeholder">Nazwisko<span class="form__placeholder--red">*</span>
                            </p>
                        </label>
                        <label for="company" class="form__label">
                            <p class="form__error">Pole nie może być puste</p>
                            <input id="company" name="company" type="text" class="form__input" placeholder="none">
                            <p class="form__placeholder">Firma<span class="form__placeholder--red">*</span>
                            </p>
                        </label>
                        <label for="position" class="form__label">
                            <p class="form__error">Pole nie może być puste</p>
                            <input id="position" name="position" type="text" class="form__input" placeholder="none">
                            <p class="form__placeholder">Stanowisko<span class="form__placeholder--red">*</span>
                            </p>
                        </label>
                        <label for="address" class="form__label">
                            <p class="form__error">Pole nie może być puste</p>
                            <input id="address" name="address" type="text" class="form__input" placeholder="none">
                            <p class="form__placeholder">Adres<span class="form__placeholder--red">*</span>
                            </p>
                        </label>
                        <label for="nip" class="form__label">
                            <p class="form__error">Pole nie może być puste</p>
                            <input id="nip" name="nip" type="text" class="form__input" placeholder="none">
                            <p class="form__placeholder">NIP<span class="form__placeholder--red">*</span>
                            </p>
                        </label>
                        <label for="phone" class="form__label">
                            <p class="form__error">Pole nie może być puste</p>
                            <input id="phone" name="phone" type="text" class="form__input" placeholder="none">
                            <p class="form__placeholder">Tel. służbowy<span class="form__placeholder--red">*</span>
                            </p>
                        </label>
                        <label for="email" class="form__label">
                            <p class="form__error">Pole nie może być puste</p>
                            <input id="email" name="email" type="text" class="form__input" placeholder="none">
                            <p class="form__placeholder">E-mail służbowy<span class="form__placeholder--red">*</span>
                            </p>
                        </label>
                        <div class="form__checkbox-wrapper">
                            <input type="checkbox" name="consent1" id="consent1" class="form__checkbox">
                            <label for="consent1" class="form__checkbox-label">Wyrażam zgodę na przetwarzanie moich
                                danych osobowych na
                                warunkach określonych w polityce
                                prywatności zamieszczonej w <a href="Regulamin.pdf" target="_blank" class="form__checkbox-link">regulaminie konkursu.</a></label>
                        </div>
                        <div class="form__checkbox-wrapper">
                            <input type="checkbox" name="consent2" id="consent2" class="form__checkbox">
                            <label for="consent2" class="form__checkbox-label">Wyrażam zgodę na otrzymywanie od
                                "LATEX"
                                informacji handlowych za pośrednictwem środków komunikacji
                                elektronicznej.</a></label>
                        </div>
                        <div class="form__checkbox-wrapper">
                            <input type="checkbox" name="consent3" id="consent3" class="form__checkbox">
                            <label for="consent3" class="form__checkbox-label">Akceptuję <a href="Regulamin.pdf" target="_blank" class="form__checkbox-link">Regulamin.</a></label>
                        </div>
                    </div>
                </div>
                <button class="form__submit" type="submit">WYŚLIJ</button>
            </form>

            <?php

            if (isset($_POST["email"])) {
                include '/home4/r2884/domains/konkurs.xpartner.pl/config/form.php';

                $firstName = $_POST['firstName'];
                $lastName = $_POST['lastName'];
                $company = $_POST['company'];
                $position = $_POST['position'];
                $address = $_POST['address'];
                $nip = $_POST['nip'];
                $phone = $_POST['phone'];
                $email = $_POST['email'];
                $hash = md5(rand(0, 1000));

                $conn = new mysqli($servername, $username, $password, $database);

                $sql = "INSERT INTO toyo (first_name, last_name, company, position, address, nip, phone, email, hash)
                        VALUES ('$firstName', '$lastName', '$company', '$position', '$address', '$nip', '$phone', '$email', '$hash')";

                if ($conn->query($sql) === TRUE) { } else { }

                $to      = $email; // Send email to our user
                $subject = 'Konkurs Bony za Opony | Weryfikacja'; // Give the email a subject 
                $message = '
 
                Cześć ' . $firstName . '!
                Dziękujemy za wzięcie udziału w konkursie.
                
                Kliknij w poniższy link, aby potwierdzić swoje zgłoszenie weryfikując adres e-mail podany w formularzu:
                http://konkurs.xpartner.pl/toyo/verify.php?email=' . $email . '&hash=' . $hash . '
                
                ';

                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                $headers .= 'From:bonyzaopony@latex.pl' . "\r\n";
                mail($to, $subject, $message, $headers);

                $conn->close();

                echo "<div class='thanks-wrapper'><div class='thanks'><img class='thanks__close' src='./dist/images/close.png'/><p class='thanks__text'>Dziękujemy za wzięcie udziałku w konkursie.<br/>Prosimy o weryfkację adresu e-mail.</p></div></div>";
            }

            ?>
        </div>
    </main>
    <footer class="footer section">
        <div class="footer__wrapper wrapper">
            <div class="footer__top">
                <p class="footer__text">Akcja trwa<br />w dniach:</p>
                <p class="footer__frame">01.08-31.12.2019</p>
            </div>
            <div class="footer__bottom">
                <a href="Regulamin.pdf" target="_blank" class="footer__link">Regulamin</a>
            </div>
        </div>
    </footer>

    <script src="./dist/js/jquery-3.4.1.min.js"></script>
    <script src="./dist/js/index.js"></script>
    <script>
        function validateForm() {
            let firstNameInput = $("#firstName");
            let lastNameInput = $("#lastName");
            let companyInput = $("#company");
            let positionInput = $("#position");
            let addressInput = $("#address");
            let nipInput = $("#nip");
            let emailInput = $("#email");
            let phoneInput = $("#phone");
            let consent1 = $('#consent1');
            let consent2 = $('#consent2');
            let consent3 = $('#consent3');

            let firstName = firstNameInput.val();
            let lastName = lastNameInput.val();
            let company = companyInput.val();
            let position = positionInput.val();
            let address = addressInput.val();
            let nip = nipInput.val();
            let email = emailInput.val();
            let phone = phoneInput.val();

            if (firstName == "" || lastName == "" ||
                email == "" || !consent1.prop("checked") || !consent2.prop("checked") || !consent3.prop("checked")) {

                if (firstName == "") {
                    firstNameInput.addClass("form__input--invalid");
                    firstNameInput.siblings(".form__error").fadeIn();
                } else {
                    firstNameInput.removeClass("form__input--invalid");
                    firstNameInput.siblings(".form__error").fadeOut();
                }

                if (lastName == "") {
                    lastNameInput.addClass("form__input--invalid");
                    lastNameInput.siblings(".form__error").fadeIn();
                } else {
                    lastNameInput.removeClass("form__input--invalid");
                    lastNameInput.siblings(".form__error").fadeOut();
                }

                if (company == "") {
                    companyInput.addClass("form__input--invalid");
                    companyInput.siblings(".form__error").fadeIn();
                } else {
                    companyInput.removeClass("form__input--invalid");
                    companyInput.siblings(".form__error").fadeOut();
                }

                if (position == "") {
                    positionInput.addClass("form__input--invalid");
                    positionInput.siblings(".form__error").fadeIn();
                } else {
                    positionInput.removeClass("form__input--invalid");
                    positionInput.siblings(".form__error").fadeOut();
                }

                if (address == "") {
                    addressInput.addClass("form__input--invalid");
                    addressInput.siblings(".form__error").fadeIn();
                } else {
                    addressInput.removeClass("form__input--invalid");
                    addressInput.siblings(".form__error").fadeOut();
                }

                if (nip == "") {
                    nipInput.addClass("form__input--invalid");
                    nipInput.siblings(".form__error").fadeIn();
                } else {
                    nipInput.removeClass("form__input--invalid");
                    nipInput.siblings(".form__error").fadeOut();
                }

                if (phone == "") {
                    phoneInput.addClass("form__input--invalid");
                    phoneInput.siblings(".form__error").fadeIn();
                } else {
                    phoneInput.removeClass("form__input--invalid");
                    phoneInput.siblings(".form__error").fadeOut();
                }


                if (email == "") {
                    emailInput.addClass("form__input--invalid");
                    emailInput.siblings(".form__error").fadeIn();
                } else {
                    emailInput.removeClass("form__input--invalid");
                    emailInput.siblings(".form__error").fadeOut();
                }

                if (!consent1.prop("checked")) {
                    consent1.addClass("form__checkbox--invalid");
                } else {
                    consent1.removeClass("form__checkbox--invalid");
                }

                if (!consent2.prop("checked")) {
                    consent2.addClass("form__checkbox--invalid");
                } else {
                    consent2.removeClass("form__checkbox--invalid");
                }

                if (!consent3.prop("checked")) {
                    consent3.addClass("form__checkbox--invalid");
                } else {
                    consent3.removeClass("form__checkbox--invalid");
                }

                return false;
            } else {
                return true;
            }

        }
    </script>
</body>

</html>